import collections
import inkex
import sys

from lxml import etree


class CleanupMarkerDefs(inkex.EffectExtension):

    ignored_attribute_keys = frozenset([
        "{http://www.inkscape.org/namespaces/inkscape}isstock",
        "{http://www.inkscape.org/namespaces/inkscape}stockid",
        "{http://www.inkscape.org/namespaces/inkscape}collect",
    ])

    def effect(self):
        dirty = False

        class CanonicalMarker(object):
            def __init__(self, id, importance):
                self.canonical_id = id
                self.canonical_importance = importance
                self.merged_ids = set()

        canonical_markers = {}

        def get_element_representation(element, mapped_element_ids=None):
            if mapped_element_ids is None:
                mapped_element_ids = {}
            
            representation = [element.tag]
            
            for attribute_key in sorted(element.attrib.keys()):
                if attribute_key in self.ignored_attribute_keys:
                    continue
                
                attribute_value = element.get(attribute_key)
                
                if attribute_key == "id":
                    attribute_value = mapped_element_ids.setdefault(attribute_value, "-mapped-id-%d" % len(mapped_element_ids))
                else:
                    for replaced_element_id, mapped_element_id in mapped_element_ids.items():
                        if attribute_value == "#%s" % replaced_element_id:
                            attribute_value == "#%s" % mapped_element_id
                        else:
                            attribute_value = attribute_value.replace(
                                "url(#%s)" % replaced_element_id,
                                "url(#%s)" % mapped_element_id)
                
                representation += [(attribute_key, attribute_value)]

            representation += [(element.text or "").strip()]
            representation += [get_element_representation(child_element, mapped_element_ids) for child_element in element.getchildren()]
            representation += [(element.tail or "").strip()]
            
            return tuple(representation)
        
        for marker_element in self.svg.xpath("svg:defs/svg:marker"):
            marker_id = marker_element.get("id")
            
            marker_importance = 0
            if marker_element.get("inkscape:isstock"):
                marker_importance += 10
            if marker_element.get("inkscape:stockid") == marker_id:
                marker_importance += 100

            marker_representation = get_element_representation(marker_element)
            if marker_representation not in canonical_markers:
                canonical_marker = CanonicalMarker(id=marker_id, importance=marker_importance)
                canonical_markers[marker_representation] = canonical_marker
            else:
                canonical_marker = canonical_markers[marker_representation]
                if marker_importance > canonical_marker.canonical_importance:
                    canonical_marker.canonical_id = marker_id
                    canonical_marker.canonical_importance = marker_importance
           
            canonical_marker.merged_ids.add(marker_id)

        mapped_marker_ids = {}
        for canonical_marker in canonical_markers.values():
            mapped_marker_ids.update((merged_id, canonical_marker.canonical_id)
                for merged_id in canonical_marker.merged_ids
                if merged_id != canonical_marker.canonical_id)

        mapped_marker_urls = dict((
            "url(#%s)" % replaced_id,
            "url(#%s)" % mapped_id) for replaced_id, mapped_id in mapped_marker_ids.items())

        for path_element in self.svg.xpath("//*[@style]"):
            marker_start = path_element.style.get("marker-start")
            if marker_start in mapped_marker_urls:
                path_element.style["marker-start"] = mapped_marker_urls[marker_start]
                dirty = True
            
            marker_mid = path_element.style.get("marker-mid")
            if marker_mid in mapped_marker_urls:
                path_element.style["marker-mid"] = mapped_marker_urls[marker_mid]
                dirty = True

            marker_end = path_element.style.get("marker-end")
            if marker_end in mapped_marker_urls:
                path_element.style["marker-end"] = mapped_marker_urls[marker_end]
                dirty = True

        for defs_element in self.svg.xpath("svg:defs"):
            for marker_element in defs_element.xpath("svg:marker"):
                if marker_element.get("id") in mapped_marker_ids:
                    defs_element.remove(marker_element)
                    dirty = True

        if dirty:
            print("Don't be alarmed if some markers appear lost!", end="\n\n", file=sys.stderr)
            print("An Inkscape bug prevents them from being displayed until the document is saved and reloaded (not just reverted).", end="\n\n", file=sys.stderr)
            print("To be safe, consider keeping a copy of the original until you've double-checked.", end="\n\n", file=sys.stderr)

            merged_marker_counts = collections.Counter(mapped_marker_ids.values())
            for merged_id in sorted(merged_marker_counts.keys()):
                print("Mapped %d identical marker defs to #%s" % (merged_marker_counts[merged_id] + 1, merged_id), file=sys.stderr)

        return dirty

    def has_changed(self, ret):
        return ret


if __name__ == "__main__":
    CleanupMarkerDefs().run()