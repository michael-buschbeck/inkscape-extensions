import argparse
import copy
import collections
import itertools
import inkex
import math
import os
import re
import sys

from lxml import etree


class ExportLayers(inkex.EffectExtension):

    def add_arguments(self, parser):
        parser.add_argument("--tab", help=argparse.SUPPRESS)
        
        # Input
        parser.add_argument("--layers",
            type=str, default="", metavar="STR[;STR...]",
            help="semicolon-separated list of label substrings of layers to export (default: all)")
        parser.add_argument("--include-locked-layers",
            type=inkex.Boolean, default=False, metavar="{true,false}",
            help="include locked layers (default: false)")
        parser.add_argument("--include-hidden-layers",
            type=inkex.Boolean, default=False, metavar="{true,false}",
            help="include hidden layers (default: false)")

        # Conversion
        parser.add_argument("--compat-resolution",
            type=inkex.Boolean, default=False, metavar="{true,false}",
            help="compatibility: improve resolution of positioning, widths, and sizes (default: false)")
        parser.add_argument("--compat-stroke-width",
            type=inkex.Boolean, default=False, metavar="{true,false}",
            help="compatibility: scale image to increase minimum stroke width above 1px at 96dpi (default: false)")
        parser.add_argument("--compat-stroke-dashoffset",
            type=inkex.Boolean, default=False, metavar="{true,false}",
            help="compatibility: bake non-zero stroke-dashoffset (default: false)")
        parser.add_argument("--compat-paint-order",
            type=inkex.Boolean, default=False, metavar="{true,false}",
            help="compatibility: bake non-default paint-order (default: false)")
        parser.add_argument("--compat-baseline-shift",
            type=inkex.Boolean, default=False, metavar="{true,false}",
            help="compatibility: bake baseline-shift for superscript and subscript (default: false)")
        parser.add_argument("--viewbox",
            choices=("drawing", "content"), default="drawing",
            help="exported layer viewbox (default: drawing)")

        # Output
        parser.add_argument("--dir",
            type=str, default=os.path.expanduser("~"),
            help="destination directory")
        parser.add_argument("--subdirs",
            type=inkex.Boolean, default=True, metavar="{true,false}",
            help="create subdirectories for nested layers (default: true)")
        parser.add_argument("--file-prefix",
            type=str, default="", metavar="PREFIX",
            help="export file name prefix")
        parser.add_argument("--overwrite",
            type=inkex.Boolean, default=True, metavar="{true,false}",
            help="overwrite existing export files (default: true)")


    def scale_user_units(self, desired_resolution=1000.0):
        """Scales user units relative to the viewbox to avoid aliasing issues
        
        Microsoft Office aliases font sizes, stroke widths, explicit shape and path coordinates,
        and individual text letter placement to integer multiples of the user unit used in the 
        drawing (nominally "px", but not the same as screen pixels). Inkscape defaults to mapping
        these user units exactly to the drawing's default unit (e.g. "mm"). This effect scales up
        user units to improve accuracy in the face of such aliasing.

        This effect is idempotent: Running it several times has the same effect as running it once.
        After the first run, the already-increased viewbox scale is kept unchanged.

        SVG bug present in Microsoft Office Version 2005 (Build 12827.20210 Click-to-Run).
        """
        scaled_style_keys = (
            "x", "y",
            "dx", "dy",
            "cx", "cy",
            "rx", "ry", "r",
            "x1", "x2", "y1", "y2",
            "width", "height",
            "points",
            "font-size",
            "stroke-width",
            "stroke-dasharray",
            "stroke-dashoffset",
            "letter-spacing",
            "word-spacing",
            "{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}cx",
            "{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}cy",
            "{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}rx",
            "{http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd}ry",
            "{http://www.inkscape.org/namespaces/inkscape}cx",
            "{http://www.inkscape.org/namespaces/inkscape}cy",
            "{http://www.inkscape.org/namespaces/inkscape}transform-center-x",
            "{http://www.inkscape.org/namespaces/inkscape}transform-center-y",
            "{http://www.inkscape.org/namespaces/inkscape}radius",
        )

        scaled_path_keys = (
            "d",
            "{http://www.inkscape.org/namespaces/inkscape}original",
            "{http://www.inkscape.org/namespaces/inkscape}original-d",
        )

        # Regex capture groups specify which parts to scale (can be multiple)
        scaled_special_cases = {
            "{http://www.inkscape.org/namespaces/inkscape}path-effect": {
                "offset_points": r"[-+\d.e]+,([-+\d.e]+)"
            }
        }

        unscaled_tags = frozenset((
            # Markers are scaled implicitly with stroke width
            "{http://www.w3.org/2000/svg}marker",
        ))

        named_font_sizes = {
            "xx-small" :  "6px",
            "x-small"  :  "8px",
            "small"    : "10px",
            "medium"   : "12px",
            "large"    : "14px",
            "x-large"  : "18px",
            "xx-large" : "24px",
        }

        def scale_value(attribute, scale):
            attribute = attribute.strip()
            if attribute == "none" or attribute == "normal" or attribute.endswith("%"):
                return attribute
            if attribute in named_font_sizes:
                attribute = named_font_sizes[attribute]
            (value, unit) = inkex.units.parse_unit(attribute)
            return inkex.units.render_unit(value * scale, "" if unit == "px" else unit)

        def scale_all_values(attribute, scale):
            return re.sub(r"[^\s,]+", lambda match: scale_value(match.group(), scale), attribute)

        def scale_all_elements(element, scale):
            if element.tag in unscaled_tags:
                return

            element_styles = inkex.AttrFallbackStyle(element)

            # Default stroke-width is 1px (not zero) and must be scaled as well
            stroke = (element_styles.get("stroke") or "none").strip()
            if stroke != "none":
                element_styles.set("stroke-width", element_styles.get("stroke-width") or "1px")

            for key in scaled_path_keys:
                path_description = element.get(key)
                if path_description:
                    path = inkex.Path(path_description)
                    path.scale(scale, scale, inplace=True)
                    element.set(key, path)
            
            for key in scaled_style_keys:
                style = element_styles.get(key)
                if style:
                    element_styles.set(key, scale_all_values(style, scale))

            if element.tag in scaled_special_cases:
                for key, value_regexp in scaled_special_cases[element.tag].items():
                    value = element.get(key)
                    if not value:
                        continue
                    value_offset = 0
                    scaled_value = ""
                    for value_match in re.finditer(value_regexp, value):
                        if value_match.lastindex is None:
                            continue
                        for group_index in range(1, value_match.lastindex + 1):
                            group = value_match.group(group_index)
                            if group is None:
                                continue
                            scaled_value += value[value_offset : value_match.start(group_index)]
                            scaled_value += scale_value(group, scale)
                            value_offset = value_match.end(group_index)
                    scaled_value += value[value_offset:]
                    element.set(key, scaled_value)

            transform = inkex.Transform(element.get("transform"))
            transform = inkex.Transform(matrix=(
                (transform.a, transform.c, transform.e * scale),
                (transform.b, transform.d, transform.f * scale)))
            element.set("transform", transform)

            for child_element in element:
                scale_all_elements(child_element, scale)

        viewbox = self.svg.get_viewbox()

        (_, _, viewbox_width, viewbox_height) = viewbox
        (document_width, _) = inkex.units.parse_unit(self.svg.get("width") or viewbox_width, "px")
        (document_height, _) = inkex.units.parse_unit(self.svg.get("height") or viewbox_height, "px")

        resolution = min(document_width / viewbox_width, document_height / viewbox_height)

        scale = 1.0
        while resolution * scale < desired_resolution:
            scale *= 10.0
        if scale == 1.0:
            return False

        for element in self.svg:
            scale_all_elements(element, scale)

        viewbox = [length * scale for length in viewbox]
        self.svg.set("viewBox", "{:.6g} {:.6g} {:.6g} {:.6g}".format(*viewbox))

        return True


    def bake_stroke_dashoffset(self):
        """Bakes non-zero "stroke-dashoffset" values into "stroke-dasharray"
        
        While Microsoft Office doesn't quite ignore "stroke-dashoffset", it applies it in odd and
        non-conforming ways. The only offset that works reliably is zero. This effect finds any
        elements with non-zero "stroke-dashoffset" and modifies the "stroke-dasharray" to produce
        the desired effect after setting "stroke-dashoffset" to zero.

        This effect is idempotent: Running it several times has the same effect as running it once.
        After the first run, only new or changed elements are modified.

        For strokes with Square and Round caps, every dash has a minimum extent due to the caps, so
        baking the dash offset only works well if the dash offset puts the path's starting point in
        a solid dash rather than the empty space between dashes. In this situation, the empty space
        with the baked dash offset will appear shorter than it ought to be.

        SVG bug present in Microsoft Office Version 2005 (Build 12827.20210 Click-to-Run).
        """
        dirty = False

        for element in self.svg.xpath("//*"):
            element_styles = inkex.AttrFallbackStyle(element)
            
            stroke_dashoffset = (element_styles.get("stroke-dashoffset") or "none").strip()
            if stroke_dashoffset == "none":
                continue
            
            (stroke_dashoffset_value, stroke_dashoffset_unit) = inkex.units.parse_unit(stroke_dashoffset)
            if stroke_dashoffset_value == 0.0:
                continue

            stroke_dasharray = (element_styles.get("stroke-dasharray") or "none").strip()
            if stroke_dasharray == "none":
                continue

            dashes = [dash_match.group() for dash_match in re.finditer(r"[^\s,]+", stroke_dasharray)]
            if not dashes:
                continue

            dirty = True

            (_, dash_unit) = inkex.units.parse_unit(dashes[0])
            dashes = [inkex.units.convert_unit(dash, dash_unit) for dash in dashes]
            dashes = list(zip(dashes, ("solid", "empty") * len(dashes)))

            dash_offset = inkex.units.convert_unit(stroke_dashoffset, dash_unit)

            while dash_offset > 0.0:
                (dash_extent, dash_type) = dashes[0]
                if dash_offset <= dash_extent:
                    dashes = [(dash_extent - dash_offset, dash_type)] + dashes[1:] + [(dash_offset, dash_type)]
                    dash_offset = 0.0
                else:
                    dashes = dashes[1:] + [dashes[0]]
                    dash_offset -= dash_extent

            (_, first_dash_type) = dashes[0]
            if first_dash_type == "empty":
                dashes = [(0.0, "solid")] + dashes
            (_, last_dash_type) = dashes[-1]
            if last_dash_type == "solid":
                dashes = dashes + [(0.0, "empty")]

            stroke_dasharray = ",".join(inkex.units.render_unit(dash_extent, dash_unit) for dash_extent, dash_type in dashes)

            element_styles.set("stroke-dasharray", stroke_dasharray)
            element_styles.set("stroke-dashoffset", "0")
        
        return dirty


    def bake_paint_order(self):
        """Bakes non-default "paint-order"

        Microsoft Office ignores the "paint-order" style and always renders in default order (first
        fill, then stroke, last markers). This effect finds any elements with non-default paint
        order and creates layered duplicates with fill, stroke, and markers set up to explicitly
        render in the intended order.

        This effect is idempotent: Running it several times has the same effect as running it once.
        After the first run, only new or changed elements are modified.

        For text, only the "paint-order" of the outer <svg:text> element is considered. If any
        nested <svg:tspan> has a "paint-order" that differs from that of its parent <svg:text>, the
        attribute value itself is retained but effectively ignored.

        SVG bug present in Microsoft Office Version 2005 (Build 12827.20210 Click-to-Run).
        """
        dirty = False

        default_paint_operation_order = (
            "fill",
            "stroke",
            "markers",
        )

        paint_operation_order_style_keys = (
            ("fill",    "fill"),
            ("stroke",  "stroke"),
            ("markers", "marker-start"),
            ("markers", "marker-mid"),
            ("markers", "marker-end"),
        )

        default_operation_order = list(default_paint_operation_order)
        default_operation_precedence = dict(zip(default_operation_order, range(len(default_operation_order))))

        for element in self.svg.xpath("//*"):
            if element.tag == "{http://www.w3.org/2000/svg}tspan":
                continue

            element_styles = inkex.AttrFallbackStyle(element)

            paint_order = element_styles.get("paint-order")
            if not paint_order or paint_order.strip() == "normal":
                continue

            operation_order = []
            for operation_match in re.finditer(r"\b(fill|stroke|markers)\b", paint_order):
                operation = operation_match.group()
                if operation not in operation_order:
                    operation_order += [operation]
            for operation in default_operation_order:
                if operation not in operation_order:
                    operation_order += [operation]

            if operation_order == default_operation_order:
                continue

            needs_operation = set()
            for operation, style_key in paint_operation_order_style_keys:
                style = element_styles.get(style_key)
                # Style being unset means it could be overwritten by <use>
                if not style or style.strip() != "none":
                    needs_operation.add(operation)

            if len(needs_operation) <= 1:
                continue

            operation_order = list(filter(lambda operation: operation in needs_operation, operation_order))

            layer_operations = []
            layer_min_precedence = len(default_operation_precedence)

            group_id = element.get("id")
            group_child_elements = []
            parent_element = element.getparent()

            def set_layer_element_id(element):
                element.set("id", group_id + "-" + "-".join(reversed(layer_operations)))

            def set_layer_element_operations(element):
                element_styles = inkex.AttrFallbackStyle(element)
                element_styles.set("paint-order", "normal")
                for operation, style_key in paint_operation_order_style_keys:
                    if operation not in layer_operations:
                        element_styles.set(style_key, "none")
            
            def set_layer_element_operations_nested(element):
                set_layer_element_operations(element)
                for child_element in element.xpath(".//*"):
                    set_layer_element_operations(child_element)

            while operation_order:
                next_operation = operation_order.pop()
                next_operation_precedence = default_operation_precedence[next_operation]

                if next_operation_precedence < layer_min_precedence:
                    layer_operations += [next_operation]
                else:
                    dirty = True

                    duplicate_element = copy.deepcopy(element)
                    parent_element.append(duplicate_element)
                    group_child_elements = [duplicate_element] + group_child_elements

                    set_layer_element_operations_nested(duplicate_element)
                    set_layer_element_id(duplicate_element)

                    layer_operations = [next_operation]

                layer_min_precedence = next_operation_precedence

            if group_child_elements:
                group_child_elements = [element] + group_child_elements

                set_layer_element_operations_nested(element)
                set_layer_element_id(element)

                group_element_index = parent_element.index(element)
                group_element = inkex.Group(attrib={"id": group_id})
                parent_element.insert(group_element_index, group_element)
                group_element.extend(group_child_elements)

        return dirty


    def bake_baseline_shift(self):
        """Bakes numeric "baseline-shift" for "sub" and "super" values
        
        Microsoft Office applies a different (greater) relative baseline shift than Inkscape for
        subscripts and superscripts. This effect replaces the generic "sub" and "super" values by
        the actual numeric values used by Inkscape.
        """
        dirty = False

        for element in self.svg.xpath("//*"):
            element_styles = inkex.AttrFallbackStyle(element)

            baseline_shift = element_styles.get("baseline-shift")
            if baseline_shift == "sub":
                element_styles.set("baseline-shift", "-20%")
                dirty = True
            elif baseline_shift == "super":
                element_styles.set("baseline-shift", "+40%")
                dirty = True

        return dirty


    def effect(self):
        """Exports layers as individual SVG files

        Each selected layer's contents and all additional elements referenced from within the layer
        are included in the layer's export file. The export file is named after the layer's label,
        and parent layer labels are either prefixed to the file name or made into subdirectories.

        Optionally, the exported drawing's viewbox can be reduced to the layer's content.
        """
        if self.options.compat_resolution:
            self.scale_user_units()
        if self.options.compat_stroke_dashoffset:
            self.bake_stroke_dashoffset()
        if self.options.compat_baseline_shift:
            self.bake_baseline_shift()
        if self.options.compat_paint_order:
            self.bake_paint_order()

        Layer = collections.namedtuple("Layer", (
            "element",
            "transform",
            "labels",
            "is_hidden",
            "is_locked",
            "has_content",
        ))

        def gather_layers(parent_element, parent_labels=(), transform=None):
            layers = []
            has_content = False

            for element in parent_element:
                if element.get("inkscape:groupmode") != "layer":
                    has_content = True
                else:
                    label = element.get("inkscape:label") or element.get("id")
                    labels = parent_labels + (label,)
                    is_hidden = element.style.get("display") == "none"
                    is_locked = element.get("sodipodi:insensitive") == "true"

                    effective_transform = inkex.Transform(transform) * element.transform
                    (nested_layers, has_nested_content) = gather_layers(element, labels, effective_transform)
                    
                    layer = Layer(
                        element=element,
                        transform=effective_transform,
                        labels=labels,
                        is_hidden=is_hidden,
                        is_locked=is_locked,
                        has_content=has_nested_content)

                    # return in Layers dialog order (top to bottom; document is bottom to top)
                    layers = [layer] + nested_layers + layers
            
            return (layers, has_content)

        (layers, _) = gather_layers(self.svg)

        if not layers:
            return False

        label_patterns = [label_pattern.strip() for label_pattern in self.options.layers.split(";")]

        def matches_label_patterns(labels):
            label_path = "/".join(labels)
            return any(label_pattern in label_path for label_pattern in label_patterns)

        reference_list_regexp = re.compile(r"\s*(?:(#\S+)|url\(\s*(#\S+)\s*\))\s*")
        reference_id_regexp = re.compile(r"#([^\s;]+)")

        def gather_element_ids(element):
            element_id = element.get("id")
            if element_id:
                yield element_id
            for child_element in element:
                yield from gather_element_ids(child_element)

        def gather_reference_ids(element):
            for _, value in itertools.chain(element.items(), element.style.items()):
                reference_list_match = reference_list_regexp.fullmatch(value)
                if reference_list_match:
                    reference_list = reference_list_match.group(1) or reference_list_match.group(2)
                    for reference_id_match in reference_id_regexp.finditer(reference_list):
                        reference_id = reference_id_match.group(1)
                        reference_element = element.root.getElementById(reference_id)
                        if reference_element is not None:
                            yield reference_id
                            yield from gather_reference_ids(reference_element)
            for child_element in element:
                yield from gather_reference_ids(child_element)

        def gather_stroke_widths(element, scale=1.0):
            element_transform = inkex.Transform(element.get("transform"))
            element_scale = scale * math.sqrt(min(
                (element_transform.a ** 2 + element_transform.c ** 2),
                (element_transform.b ** 2 + element_transform.d ** 2)))
            if element_scale > 0.0:
                element_styles = inkex.AttrFallbackStyle(element)
                element_stroke = element_styles.get("stroke") or "none"
                if element_stroke != "none":
                    element_stroke_width = element_styles.get("stroke-width") or "1px"
                    if not element_stroke_width.endswith("%"):
                        element_stroke_width_px = inkex.units.convert_unit(element_stroke_width, "px")
                        if element_stroke_width_px > 0.0:
                            yield element_stroke_width_px * element_scale
                if isinstance(element, inkex.Use):
                    yield from gather_stroke_widths(element.href, element_scale)
                else:
                    for child_element in element:
                        yield from gather_stroke_widths(child_element, element_scale)

        (_, _, viewbox_width, viewbox_height) = self.svg.get_viewbox()
        (document_width, document_width_unit) = inkex.units.parse_unit(self.svg.get("width") or viewbox_width, "px")
        (document_height, document_height_unit) = inkex.units.parse_unit(self.svg.get("height") or viewbox_height, "px")
        document_scale = min(document_width / viewbox_width, document_height / viewbox_height)

        export_count = 0

        for layer in layers:
            if not layer.has_content:
                continue
            if not self.options.include_hidden_layers and layer.is_hidden:
                continue
            if not self.options.include_locked_layers and layer.is_locked:
                continue
            if not matches_label_patterns(layer.labels):
                continue
            
            if self.options.subdirs:
                export_dir = os.path.join(self.options.dir, *layer.labels[:-1])
                export_basename = self.options.file_prefix + layer.labels[-1] + ".svg"
            else:
                export_dir = self.options.dir
                export_basename = self.options.file_prefix + "-".join(layer.labels) + ".svg"

            export_path = os.path.join(export_dir, export_basename)
            if not self.options.overwrite and os.path.exists(export_path):
                continue

            export_scale = 1.0

            if self.options.compat_stroke_width:
                min_stroke_width = round(max(0.001, min(gather_stroke_widths(layer.element, document_scale))), ndigits=3)
                while min_stroke_width * export_scale < 1.0:
                    export_scale *= 10.0

            element_ids = frozenset(gather_element_ids(layer.element))
            reference_ids = frozenset(gather_reference_ids(layer.element))

            export_defs_element = inkex.Defs()
            for def_id in reference_ids - element_ids:
                export_def_element = copy.deepcopy(self.svg.getElementById(def_id))
                export_defs_element.append(export_def_element)

            export_layer_element = copy.deepcopy(layer.element)
            export_layer_element.set("transform", str(layer.transform))

            export_document_element = inkex.SvgDocumentElement(attrib=self.svg.attrib)

            if self.options.viewbox == "content":
                bounds = inkex.BoundingBox(layer.element.bounding_box(layer.transform))
                export_document_element.set("viewBox", "{:.6g} {:.6g} {:.6g} {:.6g}".format(bounds.left, bounds.top, bounds.width, bounds.height))
                export_document_element.set("width", inkex.units.render_unit(export_scale * bounds.width * document_width / viewbox_width, document_width_unit))
                export_document_element.set("height", inkex.units.render_unit(export_scale * bounds.height * document_height / viewbox_height, document_height_unit))
            elif export_scale != 1.0:
                export_document_element.set("width", inkex.units.render_unit(export_scale * document_width, document_width_unit))
                export_document_element.set("height", inkex.units.render_unit(export_scale *  document_height, document_height_unit))

            export_document_element.append(export_defs_element)
            export_document_element.append(export_layer_element)

            export_count += 1

            if not os.path.isdir(export_dir):
                os.makedirs(export_dir)

            export_document_tree = etree.ElementTree(export_document_element)
            export_document_tree.write(export_path, encoding="utf-8")
            
            layer_friendly_path = " > ".join(layer.labels)

            export_byte_size = os.path.getsize(export_path)
            if export_byte_size >= 1024 * 1024:
                export_friendly_size = "%.1f MB" % (export_byte_size / float(1024 * 1024))
            elif export_byte_size >= 1024:
                export_friendly_size = "%.1f kB" % (export_byte_size / float(1024))
            else:
                export_friendly_size = "%d Bytes" % export_byte_size

            (export_width, export_width_unit) = inkex.units.parse_unit(export_document_element.get("width"))
            (export_height, export_height_unit) = inkex.units.parse_unit(export_document_element.get("height"))

            print(layer_friendly_path, file=sys.stderr)
            print("    Size:  %s x %s (%s) at %gx (resize to %g%%)" % (
                inkex.units.render_unit(export_width / export_scale, export_width_unit),
                inkex.units.render_unit(export_height / export_scale, export_height_unit),
                export_friendly_size,
                export_scale,
                100.0 / export_scale), file=sys.stderr)
            print("    File:  %s" % os.path.relpath(export_path, self.options.dir), file=sys.stderr)
            print(file=sys.stderr)

        print("Exported 1 layer." if export_count == 1 else "Exported %d layers." % export_count, file=sys.stderr)
        return True

    def has_changed(self, ret):
        return False


if __name__ == "__main__":
    ExportLayers().run()